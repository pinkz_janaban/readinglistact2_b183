//console.log("Hello world")

//#1
let myWord = prompt("What is your name?")
let myName = '';

for(i=0; i < myWord.length; i++){
	if(myWord[i].toLowerCase() == 'a' || myWord[i].toLowerCase() == 'e'|| myWord[i].toLowerCase() == 'i' || myWord[i].toLowerCase() == 'o' || myWord[i].toLowerCase() == 'u'){
		
		myName += 3;
		console.log("This is myName inside if: " +myName);
	}
	else{
		myName += myWord[i];
		console.log("This is myName inside else: " +myName);
	}
}

//#2

let students = ["John", "Jane", "Joe"];
//Total size of an array
console.log(students.length);
//Adding an new element at the beginning of an array
students.unshift("Jack");
console.log(students);

//Adding a new element at the end of an array
students.push("Jill");
console.log(students);

//Removing an existing element at the beginning of an array
students.splice(0,1);
console.log(students);

//Removing an existing element at the end of an array
students.pop();
console.log(students);

//Find the index number of the last element in an array
console.log(students.length-1);

//Print each element of the array in the browser console.
console.log(students[0]);
console.log(students[1]);
console.log(students[2]);


//3
console.log(students)
const index = students.indexOf("Jane");
if (index > -1) {
  students.splice(index, 1); 
}

console.log(students)



//4

let person = {};
person.firstName = "Juan",
person.lastName = " Dela Cruz",
person.nickName = "J",
person.age = 25,
person.address = {
		city: "Quezon City",
		country: "Philippines"
	},
person.friends = ["John", " Jane", " Joe"],
person.fullName = function() {
    	console.log("My name is " + this.firstName + " " + this.lastName + ", but you can call me " + this.nickName + ". I am " + this.age + " years old and I live in " + this.address.city + ", " + this.address.country + ". My friends are " + this.friends + ".") ;
  } 

person.fullName()


//5
/*Apply the JavaScript Destructuring Assignment in the object variable
Use JavaScript Template literals in displaying the text inside the function property of the object
*/

let person = {
firstName: "Juan",
lastName: " Dela Cruz",
nickName: "J",
age: 25,
address: {
		city: "Quezon City",
		country: "Philippines"
	},
friends: ["John", " Jane", " Joe"],
fullName: () => {
    	return `My name is ${firstName} ${lastName}, but you can call me ${nickName}. I am ${age} years old and I live in ${address.city}, ${address.country}. My friends are ${friends}.`;
	}
}

console.log(person.fullName);


